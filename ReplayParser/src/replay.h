﻿#pragma once

#include <string>
#include <vector>
#include <fstream>
#include "point/point.h"
#include "game/game.h"

using spud_base::point::Point;
using spud_base::game::GameMode;
using spud_base::game::ModCombo;

namespace replay_parser {
    enum KeyPress {
        kKeyM1 = 1,
        kKeyM2 = 2,
        kKeyK1 = 4,
        // kKeyK1 is always used with kKeyM1 (always = 5)
        kKeyK2 = 8,
        // kKeyK2 is always used with kKeyM2 (always = 10)
        kKeySmoke = 16
    };

    struct ReplayTick {
        int64_t time_since_last_tick;
        int64_t time_since_beginning;
        Point<double> cursor_coord;
        KeyPress keys_pressed;

        ReplayTick(int64_t since_last, int64_t since_beginning, Point<double> coord, KeyPress keys) :
            time_since_last_tick{ since_last },
            time_since_beginning{ since_beginning },
            cursor_coord{ coord },
            keys_pressed{ keys } { }
    };

    class Replay {
    public:
        // TODO: move GameMode and ModCombo into spud_base

        GameMode mode;
        int32_t game_version;
        std::string beatmap_hash;
        std::string player_name;
        std::string replay_hash;
        int16_t count_300;
        int16_t count_100;
        int16_t count_50;
        int16_t count_geki;
        int16_t count_katu;
        int16_t count_misses;
        int32_t total_score;
        int highest_combo; // TODO: be more specific than 'int' ?
        bool perfect_combo; // full combo
        ModCombo mods;
        std::string life_bar_raw; // TODO: parse this, make result public, privatize this one
        int64_t date_played;
        std::vector<ReplayTick> replay_ticks;

        Replay(const std::string& replay_path); // calls Load() method in constructor
        Replay() = default;

        // loads file into memory
        // public so the user can load after using default constructor
        void Load(const std::string& replay_path);

        // returns accuracy as a percentage between 0 and 1
        double Accuracy() const;

        // prints lots of information about replay
        void PrintInfo() const;
        // stream overload for quick information
        friend std::ostream& operator<<(std::ostream& stream, const Replay& replay);

        // for manual confirmation of replay ticks
        void WriteReplayTicksToFile(std::string output_name = "default") const;

        // exports bytes into a plain text file, formatted for human readability
        void WriteRawBytesToFile();

        // parse the replay and store information in Replay class public members
        void ParseReplay();

        // Beatmap GetBeatmap();
    private:
        std::string replay_path_;
        std::ifstream file_;
        std::streamoff file_size_;
        std::vector<uint8_t> file_data_; // bytes read from file_
        std::vector<uint8_t>::iterator file_position_; // position in file_data_
        size_t file_pos_; // numerical representation of file_position_, for use like file_data[file_pos_]
        
        // whether the replay file has been loaded into memory yet
        bool loaded_ = false; // would be replay_loaded_ if we implement beatmap loading

        // size of lzma compressed replay ticks, this is assigned when parsing the replay
        int32_t compressed_data_length_;

        // Beatmap beatmap_;
        // bool beatmap_loaded_;
        // bool LoadBeatmap();

        // methods for reading bytes from file_data_
        // NOTE: these used to read directly from the ifstream file_, the new way is probably
        //      a little slower but easier to work with
        uint8_t ReadByte();
        int16_t ReadShort();
        int32_t ReadInt();
        int64_t ReadLong();
        std::string ReadString();

        void ParseUncompressedReplayData(std::string& uncompressed_data);

        void CleanReplayTicks(); // gets rid of meaningless and outlier replay ticks
    };

    inline std::ostream& operator<<(std::ostream& stream, const Replay& replay) {
        stream << "Play by " << replay.player_name << ", " << replay.Accuracy() * 100 << "% acc";
        return stream;
    }
}
