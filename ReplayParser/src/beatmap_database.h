﻿#pragma once
#include <string>
#include <fstream>
#include <unordered_map>

// TODO: move beatmap_database to GameplayProcessor project since that's where it's mainly used?
// it doesn't have any current use in parsing replays unless we want to get title/artist/etc. information about the map

namespace replay_parser {
    // could have a DatabaseValue struct to store more information in value than just relative path

    struct DatabaseEntry {
		std::string file_hash; // md5 hash of the .osu file
		std::string relative_path; // relative to the osu! songs folder
    };

    //TODO: for the beatmap database, add a header line that is the songs_folder path when doing GenerateDbFile()
    //      and then in LoadDbFile(), check that the header line is equal to the expected songs_folder

	class BeatmapDatabase {
	public:
        // constructor takes osu songs folder path and an optional db file name
        BeatmapDatabase(std::string songs_folder, std::string file_name = "spudbm.db")
	        : database_file_name_{file_name}, songs_folder_path_{songs_folder} {};

        bool LoadDbFile();
        bool GenerateDbFile();

        std::string Get(std::string beatmap_hash) const;
	private:
		std::string database_file_name_; // maybe use database_file_path_ instead?
		std::ifstream input_file_;
		std::ofstream output_file_;

        std::string songs_folder_path_;

		std::unordered_map<std::string, std::string> database_;
	};
}
