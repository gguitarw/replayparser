﻿#pragma once

#include <Windows.h>
#include <wincrypt.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include <iomanip>

namespace replay_parser {
    namespace util {
        // TODO: move this to a more general util, not strictly related to replay_parser
        inline std::string BytesToHex(uint8_t* bytes, size_t size) {
            // https://stackoverflow.com/a/10600155 for printing hex stream
            std::stringstream out;
            for (size_t i = 0; i < size; ++i) {
                out << std::hex << std::setfill('0') << std::setw(2) << bytes[i] << ' ';
            }
            // out << std::endl;
            return { out.str() };
        }

        // input is the first byte to be read
        // returns the decoded integer as well as how many bytes were read
        inline std::pair<int64_t, size_t> DecodeUleb128(uint8_t* input) {
            int64_t value = 0;
            uint8_t shift = 0;
            size_t bytes_read = 0;

            while (true) {
                // file.read(reinterpret_cast<char*>(&byte_buffer), 1);
                uint8_t byte_buffer = *input;
                ++input;
                ++bytes_read;

                // 0x7F -> 0b01111111 (bitmask to get rid of MSB)
                // The << is to shift left 7 times for each byte that we've read
                value |= (byte_buffer & 0x7FULL) << shift;

                // break if the MSB is 0
                if (!(byte_buffer & 0x80)) {
                    break;
                }

                shift += 7;
            }

            return { value, bytes_read };
        }

        std::string DecodeLzmaFile(uint8_t* data_stream, size_t num_bytes) {
            // check that lzma.exe exists at C:\Util\lzma.exe
            std::string lzma_exe = R"(C:\Util\lzma.exe)";
            if (GetFileAttributes(lzma_exe.c_str()) == INVALID_FILE_ATTRIBUTES) {
                // lzma.exe wasn't found there
                return "";
            }

            // write compressed contents to file
            std::string tmp_file_name = "tmpreplay.lzma";
            std::ofstream tmp_file(tmp_file_name, std::ios::out | std::ios::binary);
            tmp_file.write(reinterpret_cast<const char*>(data_stream), num_bytes);
            tmp_file.close();

            // create a new process to run lzma decompress on the compressed replay data file
            std::string output_file_name = "tmpreplay.bin";
            // std::string lzma_exe = "lzma.exe";
            std::string lzma_args = " d " + tmp_file_name + " " + output_file_name;
            std::string open_string = "open";

            // run lzma.exe
            // https://msdn.microsoft.com/en-us/library/windows/desktop/bb759784(v=vs.85).aspx
            // to get the lzma.exe, download the lzma sdk from https://www.7-zip.org/sdk.html
            //      then compile LzmaCon from CPP\7zip\Bundles\LzmaCon\LzmaCon.dsp to C:\Util\lzma.exe
            // note: you will probably need to retarget solution and change project output name/directory
            SHELLEXECUTEINFO sei;
            sei.cbSize = sizeof(SHELLEXECUTEINFO);
            sei.fMask = SEE_MASK_NOCLOSEPROCESS; // wait for the process to close, then close the handle
            sei.hwnd = NULL;
            sei.lpVerb = open_string.c_str();
            sei.lpFile = lzma_exe.c_str();
            sei.lpParameters = lzma_args.c_str();
            sei.lpDirectory = NULL;
            sei.nShow = SW_HIDE;

            ShellExecuteEx(&sei);

            // sync with the exit of lzma.exe
            WaitForSingleObject(sei.hProcess, INFINITE);
            CloseHandle(sei.hProcess);

            // read the decompressed data into memory
            std::ifstream decompressed_data_file(output_file_name, std::ios::in);
            // lazily wait until the tmpreplay.bin is ready to be read
            while (!decompressed_data_file.is_open()) {
                decompressed_data_file.open(output_file_name, std::ios::in);
                Sleep(2);
            }

            std::string decompressed_data;
            while (decompressed_data_file.good()) {
                // TODO: do this all in one go, not in a loop. file should be all on one line so a single getline should suffice
                std::string buffer;
                std::getline(decompressed_data_file, buffer);
                decompressed_data.append(buffer);
            }

            decompressed_data_file.close();

            std::remove(tmp_file_name.c_str());
            std::remove(output_file_name.c_str());

            return decompressed_data;
        }

        // pass in all bytes of lzma compressed data (including header)
        inline std::string DecodeLzma(uint8_t* data_stream, size_t num_bytes) {
            return DecodeLzmaFile(data_stream, num_bytes);
        }

        // wrapper for the DecodeLzma above that takes a vector instead of byte pointer
        inline std::string DecodeLzma(std::vector<uint8_t>& data) {
            uint8_t* data_ptr = data.data();
            const size_t size = data.size();
            return DecodeLzma(data_ptr, size);
        }

        inline std::string Md5FileHash(const std::string& file_path) {
            // https://msdn.microsoft.com/en-us/library/windows/desktop/aa382380(v=vs.85).aspx
            // crypto setup, TODO: Implement checks for these functions failing
            HCRYPTPROV crypt_provider = 0;
            HCRYPTHASH crypt_hash = 0;
            CryptAcquireContext(&crypt_provider, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
            CryptCreateHash(crypt_provider, CALG_MD5, 0, 0, &crypt_hash);

            // open file handle, TODO: use CreateFile() instead of CreateFileA()? Is this really needed if not using unicode?
            const HANDLE file_handle = CreateFileA(file_path.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL,
                                                   OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            // setup input buffer for reading the file and CryptHashData()
            BYTE byte_buffer[1024] = {0};
            DWORD bytes_read = 0;

            // continuously hash file contents line by line
            while (ReadFile(file_handle, byte_buffer, 1024, &bytes_read, NULL)) {
                if (bytes_read == 0) {
                    break;
                } // didn't read any more than the last time, we're done with the file
                CryptHashData(crypt_hash, byte_buffer, bytes_read, 0);
            }

            // retreive the hash and get ready to store it in a string for returning
            BYTE raw_hash_bytes[16];
            DWORD data_size = 16;
            CryptGetHashParam(crypt_hash, HP_HASHVAL, raw_hash_bytes, &data_size, 0);

            // store the hash in a string (stream)
            std::stringstream hash_string_stream;
            for (const auto& raw_hash_byte : raw_hash_bytes) {
                // check for a leading zero that would be cut off by std::hex
                if (static_cast<int>(raw_hash_byte) < 16) {
                    hash_string_stream << '0';
                }
                hash_string_stream << std::hex << static_cast<int>(raw_hash_byte);
            }

            // close handles
            CloseHandle(file_handle);
            CryptDestroyHash(crypt_hash);
            CryptReleaseContext(crypt_provider, 0);

            return hash_string_stream.str();
        }
    }
}
