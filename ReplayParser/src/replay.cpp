﻿#pragma warning( disable : 4996 ) // for C4996, related to std::copy with "unsafe parameters"

#include "replay.h"
#include <iostream>
#include "util.h"
#include "strings/string_utils.h"
#include <iterator>
#include <iomanip>

using spud_base::util::PadSpaces;
using spud_base::util::SplitString;

replay_parser::Replay::Replay(const std::string& replay_path) : replay_path_(replay_path) {
    Load(replay_path);
}

void replay_parser::Replay::Load(const std::string& replay_path) {
    if (loaded_) return; // already loaded, don't try again

    replay_path_ = replay_path;

    file_.open(replay_path, std::ios::binary | std::ios::in);
    if (!file_.is_open()) {
        return;
    }

    // stop eating new lines in binary mode
    file_.unsetf(std::ios::skipws);

    // get file size
    file_.seekg(0, std::ifstream::end);
    file_size_ = file_.tellg();
    file_.seekg(0, std::ifstream::beg);

    // reserve memory for reading file bytes
    file_data_.reserve(file_size_);

    // inspired by https://stackoverflow.com/questions/15138353/how-to-read-a-binary-file-into-a-vector-of-unsigned-chars
    file_data_.insert(file_data_.begin(),
                      std::istream_iterator<uint8_t>(file_),
                      std::istream_iterator<uint8_t>());
    // istream_iterator internally uses >> operator, might be slow

    file_position_ = file_data_.begin();

    file_.close();
    loaded_ = true;
}

void replay_parser::Replay::WriteRawBytesToFile() {
    if (!loaded_) return;

    std::ofstream out(replay_path_ + ".hr", std::ios::out);

    /* formatted like:
    mode = 0, num_bytes = 1
00
    game_version = 20151228, num_bytes = 4
bc 7b 33 01 // remember, these are little-endian, so read int32 as 01 33 7b bc
    ...
     */

    std::string line;

    line = util::BytesToHex(&*file_position_, 1);
    std::stringstream outstream;
    for (size_t i = 0; i < 1; ++i) {
        outstream << std::hex << std::setfill('0') << std::setw(2) << file_position_[i] << ' ';
        std::cout << outstream.str() << std::endl;
    }
    std::cout << "mode = " << ReadByte() << '\n' << outstream.str() << std::endl;

    line = util::BytesToHex(&*file_position_, 4);
    std::stringstream outstream2;
    for (size_t i = 0; i < 4; ++i) {
        outstream2 << std::hex << std::setfill('0') << std::setw(2) << file_position_[i] << ' ';
        std::cout << std::hex << outstream2.str() << std::endl;
    }
    std::cout << "game_version = " << ReadInt() << '\n' << outstream2.str() << std::endl;

    out.close();
}

void replay_parser::Replay::ParseReplay() {
    mode = static_cast<GameMode>(ReadByte());
    game_version = ReadInt();
    beatmap_hash = ReadString();
    player_name = ReadString();
    replay_hash = ReadString();
    count_300 = ReadShort();
    count_100 = ReadShort();
    count_50 = ReadShort();
    count_geki = ReadShort();
    count_katu = ReadShort();
    count_misses = ReadShort();
    total_score = ReadInt();
    highest_combo = ReadShort();
    perfect_combo = static_cast<bool>(ReadByte());
    mods = static_cast<ModCombo>(ReadInt());
    life_bar_raw = ReadString();
    date_played = ReadLong();
    compressed_data_length_ = ReadInt();

    std::string replay_data_string = util::DecodeLzma(&*file_position_, compressed_data_length_);
    ParseUncompressedReplayData(replay_data_string);
    CleanReplayTicks();
}

double replay_parser::Replay::Accuracy() const {
    // TODO: implement other modes than standard
    return static_cast<double>(300 * count_300 + 100 * count_100 + 50 * count_50)
        / static_cast<double>(300 * (count_300 + count_100 + count_50 + count_misses));
}

void replay_parser::Replay::PrintInfo() const {
    std::cout << "Game mode: " // << mode
        << "\nGame version: " << game_version
        << "\nBeatmap hash: " << beatmap_hash
        << "\nPlayer name: " << player_name
        << "\nReplay has: " << replay_hash
        << "\n300s: " << count_300
        << "\n100s: " << count_100
        << "\n50s: " << count_50
        << "\nGekis: " << count_geki
        << "\nKatus: " << count_katu
        << "\nMisses:" << count_misses
        << "\nTotal score: " << total_score
        << "\nHighest combo: " << highest_combo
        << "\nPerfect combo (0/1): " << perfect_combo
        << "\nMods bitmask: " // << mods
        << "\nDate played: " << date_played
        << std::endl;
}

void replay_parser::Replay::WriteReplayTicksToFile(std::string output_name) const {
    if (output_name == "default") {
        output_name = player_name + " - " + beatmap_hash + " (" + std::to_string(date_played) + ").osrt"; // osu replay ticks
    }

    std::ofstream output_file(output_name, std::ios::out);

    // write header line to file
    std::string header;
    const size_t column_size = 30;
    header.append(PadSpaces("SinceLastTick", column_size));
    header.append(PadSpaces("SinceBeginning", column_size));
    header.append(PadSpaces("MouseCoordinate", column_size));
    header.append("KeysPressed\n");
    output_file.write(header.c_str(), header.size());

    // write main contents of file
    for (const auto& tick : replay_ticks) {
        std::string line;
        line.append(PadSpaces(std::to_string(tick.time_since_last_tick), column_size));
        line.append(PadSpaces(std::to_string(tick.time_since_beginning), column_size));
        line.append(PadSpaces(tick.cursor_coord.ToString(), column_size));
        line.append(PadSpaces(std::to_string(tick.keys_pressed), column_size) + '\n');

        output_file.write(line.c_str(), line.size());
    }

    output_file.close();
}

void replay_parser::Replay::ParseUncompressedReplayData(std::string& uncompressed_data) {
    std::vector<std::string> split_data = SplitString(uncompressed_data, ',');

    int64_t time = 0;
    
    for (const auto& line : split_data) {
        std::vector<std::string> tick_data = SplitString(line, '|');

        if (tick_data.size() < 3) continue;

        time += std::stoi(tick_data.at(0));

        // reference osu wiki to see how this information is laid out in the replay file
        replay_ticks.emplace_back(
            std::stoi(tick_data.at(0)),
            time,
            Point<double>(std::stod(tick_data.at(1)),
                std::stod(tick_data.at(2))),
            static_cast<KeyPress>(std::stoi(tick_data.at(3))));
    }
}

// TODO: do something about the code reuse here
uint8_t replay_parser::Replay::ReadByte() {
    // uint8_t r{ *reinterpret_cast<uint8_t*>(&(*file_position_)) };
    uint8_t r;
    std::copy(file_position_, file_position_ + 1, reinterpret_cast<uint8_t*>(&r));
    file_position_ += 1;
    return r;
}

int16_t replay_parser::Replay::ReadShort() {
    // int16_t r{ *reinterpret_cast<uint8_t*>(&(*file_position_)) };
    uint16_t r;
    std::copy(file_position_, file_position_ + 2, reinterpret_cast<uint8_t*>(&r));
    file_position_ += 2;
    return r;
}

int32_t replay_parser::Replay::ReadInt() {
    // int32_t r{ *reinterpret_cast<uint8_t*>(&(*file_position_)) };
    uint32_t r;
    std::copy(file_position_, file_position_ + 4, reinterpret_cast<uint8_t*>(&r));
    file_position_ += 4;
    return r;
}

int64_t replay_parser::Replay::ReadLong() {
    // int64_t r{ *reinterpret_cast<uint8_t*>(&(*file_position_)) };
    uint64_t r;
    std::copy(file_position_, file_position_ + 8, reinterpret_cast<uint8_t*>(&r));
    file_position_ += 8;
    return r;
}

std::string replay_parser::Replay::ReadString() {
    // PART 1: A single byte, 0x00 or 0x0b. 0x0b indicates the other two parts exist
    uint8_t r = *file_position_;
    ++file_position_;
    if (r != 0x0b) {
        return "";
    } // otherwise, r == 0x0b

    // PART 2: Contains a variable length ULEB128 encoded integer that indicates how many bytes part 3 is
    //		pseudocode to decode: https://en.wikipedia.org/wiki/LEB128#Decode_unsigned_integer
    const auto result = util::DecodeUleb128(&(*file_position_));
    const uint32_t bytes_in_string = static_cast<uint32_t>(result.first);
    file_position_ += result.second;

    // PART 3: The string itself, encoded in UTF-8
    std::string decoded_string = { file_position_, file_position_ + bytes_in_string };
    file_position_ += bytes_in_string;
    return decoded_string;
}

void replay_parser::Replay::CleanReplayTicks() {
    // something didn't work when getting replay_ticks, say something about it
    if (replay_ticks.size() <= 3) {
        std::cout << "replay_ticks is too small to have been parsed correctly, go fix it" << std::endl;
        return;
    }

    // get rid of the first few (initialization) ticks, and the last tick
    replay_ticks.erase(replay_ticks.begin(), replay_ticks.begin() + 2);
    replay_ticks.erase(replay_ticks.end() - 1);

    // TODO: get rid of replay ticks before the user skips the song intro?
    // we might want that for moving the cursor before the song begins though

    // TODO: check for large instant movements

    // TODO: check for offscreen cursor positions
}
