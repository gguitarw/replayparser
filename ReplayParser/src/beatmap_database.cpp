﻿#include "beatmap_database.h"
#include <Windows.h>
#include "util.h"
#include "strings/string_utils.h"

bool replay_parser::BeatmapDatabase::LoadDbFile() {
    if (output_file_.is_open()) {
        return false;
    }

    // start by clearing the loaded memory and loading the database file
    database_.clear();
    input_file_.open(database_file_name_, std::ios::in);

    if (!input_file_.is_open()) {
        // couldn't open database file
        // TODO: should it GenerateDbFile() here and retry? or leave that up to the user
        return false;
    }

    // for each line in the database file, read it into the unordered_map in memory
    std::string line;
    while (std::getline(input_file_, line)) {
        if (line.size() < 5) {
            continue;
        } // get rid of empty lines if there are any
        
        std::vector<std::string> split_line = spud_base::util::SplitStringAtFirst(line, '.');
        database_.emplace(split_line.at(0), split_line.at(1));
    }

    input_file_.close();
    return true;
}

bool replay_parser::BeatmapDatabase::GenerateDbFile() {
    if (input_file_.is_open()) {
        return false;
    }

    // start by deleting and then opening the database file to start fresh
    // TODO: more elegant way to do this?
    std::remove(database_file_name_.c_str());
    output_file_.open(database_file_name_, std::ios::out);

    // read all beatmap folders in songs folder, look for .osu files, store in database
    WIN32_FIND_DATAA folder_data;
    std::string folder_search = songs_folder_path_ + "*";

    HANDLE folder_handle = FindFirstFileA(folder_search.c_str(), &folder_data);

    if (folder_handle == INVALID_HANDLE_VALUE) {
        // found no first file/folder, something is wrong
        return false;
    }

    do {
        if (!(folder_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
            // found something that's not a folder
            continue;
        }

        WIN32_FIND_DATAA file_data;
        std::string file_search;

        // search for .osu files
        file_search = songs_folder_path_ + folder_data.cFileName + "\\*.osu";
        HANDLE file_handle = FindFirstFileA(file_search.c_str(), &file_data);

        if (file_handle != INVALID_HANDLE_VALUE) {
            // found a .osu file
            // TODO: streamline this with the DatabaseEntry struct, with constructor and ToString() method
            std::string file_hash = util::Md5FileHash(songs_folder_path_ + folder_data.cFileName + "\\" + file_data.cFileName);
            std::string database_entry = file_hash + "." + folder_data.cFileName + "\\" + file_data.cFileName + '\n';
            output_file_.write(database_entry.c_str(), database_entry.size());
        }

        FindClose(file_handle);
    } while (FindNextFileA(folder_handle, &folder_data));

    FindClose(folder_handle);
    output_file_.close();
    return true;
}

std::string replay_parser::BeatmapDatabase::Get(std::string beatmap_hash) const {
    // search the unordered map for the maphash key, return the associated file path
    // not using .at() because of https://stackoverflow.com/a/19191284
    auto path = database_.find(beatmap_hash); // returns the iterator instead of reference to value
    if (path == database_.end()) {
        return "";
    }

    // could do another check here that path->first is the same as beatmap_hash

    return path->second; // first in the key, second is the value
}
