#include "pch.h"
#include "replay.h"
#include "game/game.h"

namespace rp = replay_parser;
using rp::Replay;

TEST(ReplayParse, cookiezi_freedom_dive) {
    Replay replay(R"(replays\Cookiezi - Freedom Dive HDHR.osr)");
    // replay.WriteRawBytesToFile();
    replay.ParseReplay();

    EXPECT_EQ(replay.mode, spud_base::game::kModeStandard);
    EXPECT_EQ(replay.beatmap_hash, "da8aae79c8f3306b5d65ec951874a7fb");
    EXPECT_EQ(replay.player_name, "Cookiezi");
    EXPECT_DOUBLE_EQ(replay.replay_ticks.at(25).cursor_coord.x, 304.697900);
}

TEST(ReplayParse, cookiezi_remote_control) {
    Replay replay(R"(replays\Cookiezi - New Remote Control.osr)");
    replay.ParseReplay();

    EXPECT_EQ(replay.mode, GameMode::kModeStandard);
    EXPECT_EQ(replay.beatmap_hash, "d7e1002824cb188bf318326aa109469d");
    EXPECT_EQ(replay.player_name, "Cookiezi");
    EXPECT_DOUBLE_EQ(replay.replay_ticks.at(106).cursor_coord.y, 136.817400);
    // EXPECT_EQ(replay.replay_ticks.at(106).keys_pressed,
              // rp::kKeyK1 & rp::kKeyM1 & rp::kKeyK2 & rp::kKeyM2);
}